---
layout: post
title:  "Les Bipodocrates vous remercient et vous disent à bientôt ici ou ailleurs"
date:   2024-08-23
categories: manif bretagne remerciements
---
# Email adressé aux parties prenantes de la manifestation à Plougrescant

Nous, bipodocrates, tenions à remercier toutes les parties prenantes de la manifestation contre le déni de démocratie qui s'est tenue jeudi 22/08/2024 à Plougrescant.

Un grand merci evidemment aux 30 citoyen-ne-s qui ont foulé le pavé plougrescantais pour clamer haut et fort leur attachement à la démocratie et alerter sur la dérive autocratique à laquelle on assiste

Un merci également à Madame le Maire et son équipe pour avoir permis que cette manifestation se tienne dans de bonnes conditions. Et surtout pour avoir rédigé l'arrêté municipal 2024-67 qui pour la première fois dans l'histoire de l'humanité a mentionné explicitement le collectif Les Bipodocrates, lui donnant ainsi une existence légale.

Un merci tout particulier à la brigade de gendarmerie de Tréguier et son major qui, en envoyant 2 voitures, a par la même rendu crédible notre événement.

Un merci enfin aux journalistes du Télégramme et de Radio Croquettes (qui ont couvert l'événement dans son intégralité) ainsi que ceux du Trégor et de Ouest France, qui par leur travail, font rayonner l'âme bipodocrate.

Nous vous invitons à un apéro citoyen ce vendredi 23/08/2024 pour réfléchir à ce qu'on peut faire chacun à son niveau face à l'urgence démocratique, et vous disons à bientôt ici ou ailleurs pour de nouvelles aventures !

Bipodocratement vôtre,

Le collectif les Bipodocrates

PS : la bipodocratie tire son nom de la loi des deux pieds, qui régit [les forums ouverts](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert)
