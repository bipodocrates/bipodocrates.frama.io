---
layout: post
title:  "Contre la réforme des retraites"
date:   2023-01-19
categories: manifestation alsace
---
Première manifestation des Bipodocrates le 19/01/2023 contre la réforme des retraites de Macron dans les rues d'Itterswiller (Bas-Rhin)

Les Dernières Nouvelles d'Alsace ont couvert l'événement :
[Du jamais vu dans le petit village viticole](https://www.dna.fr/politique/2023/01/19/manifestation-a-itterswiller-du-jamais-vu-dans-le-petit-village-viticole)

![bipodocrates en manif à Itterswiller](/assets/photo-1674160247.webp "Bipodocrates en manif")
