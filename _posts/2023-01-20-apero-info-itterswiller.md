---
layout: post
title:  "Apéro-Info à Itterswiller"
date:   2023-01-20
categories: alsace apero-info
---
Terminée la réunion publique avec un sachant à la tribune et les habitants qui écoutent religieusement la bonne parole. Les bipodocrates, après leur manifestation réussie de jeudi dernier, organisaient un "apéro-info" et là encore, ils ont créé la surprise !

![pleinierebipodocrates en pleiniere](/assets/en-cercle-itterswiller.jpg "Bipodocrates en pleiniere")

La diffusion de tracts au supermarché du coin et le porte-à-porte dans les rues d'Itterswiller ont porté leurs fruits. 14 citoyens d'Itterswiller et d'ailleurs ont répondu présent à l'appel des bipodocrates pour s'informer sur la réforme des retraites. L'idée à ce stade n'était pas de convaincre, mais de s'entraider entre citoyens pour trouver des réponses fiables et sourcées aux questions qu'ils se posaient.

Contrairement aux réunions publiques classiques, chaque citoyen a pu s'exprimer, exposer ses interrogations en petit groupe ou en pléniere.

![](/assets/aurore-aurelie-small.jpg)

![](/assets/bernard-and-co-small.jpg)


2 bipodocrates avaient planché en amont et ainsi ont pu répondre aux principales questions sur le contenu de la réforme. Certaines questions plus pointues nécessitaient davantage de recherche. Elles ont été mises au "frigo". Non pas pour les évacuer, mais pour bien les conserver et y répondre une fois l'info précise trouvée.

C'est là qu'on comprend mieux à quoi sert les bipodocrates : **créer des espaces émancipateurs pour les citoyens**, en s'informant activement et collectivement (plutôt qu'en consommant de l'info) à l'aide de cet "apéro-info" ou en s'appropriant la rue le temps d'une manifestation.


