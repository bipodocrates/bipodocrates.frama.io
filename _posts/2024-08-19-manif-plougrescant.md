---
layout: post
title:  "Mobilisons nous contre le déni de démocratie de Macron : Tous à Plougrescant !"
date:   2024-08-19
categories: manifestation bretagne
---

![bipodocrates en manif à Itterswiller](/assets/photo-1674160247.webp "Bipodocrates en manif")

**A noter dans votre agenda**

[Après leur manifestation à Itterswiller (Bas-Rhin) en janvier 2023 contre la réforme des retraites](https://www.dna.fr/politique/2023/01/19/manifestation-a-itterswiller-du-jamais-vu-dans-le-petit-village-viticole), les Bipodocrates débarquent à Plougrescant (Côtes d'Armor) pour une manifestation **ce jeudi 22/08/2024 à 14h30** contre le déni de démocratie de Macron suite au résultat des élections législatives.

Le collectif demande instamment que le Président de la République Emmanuel Macron respecte le choix des français-e-s en désignant la première ministre proposée par le Nouveau Front Populaire arrivé en tête des élections législatives.

Rendez-vous pour manifester tous ensemble à 14h30 place de la mairie de Plougrescant !
