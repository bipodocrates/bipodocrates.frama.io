---
layout: post
title:  "Apéro citoyen à Plougrescant"
date:   2024-08-20
categories: apero bretagne
---

![bipodocrates en apero à Itterswiller](/assets/en-cercle-itterswiller.jpg "Bipodocrates en apero")

**A noter dans votre agenda**


Dans le prolongement de la manifestation de jeudi, nous, bipodocrates, organisons vendredi 23/08 à 17h un apéro citoyen sur le thème **Que faire face à l'urgence démocratique ?**
Chaque citoyen-n-e pourra s'exprimer, être écouté, et nous créerons les conditions pour réfléchir et décider ensemble des éventuelles actions à mettre en place.

L'apéro citoyen aura lieu le 23/08 à 17h à Plougrescant en face de l'église au 5 hent sant gonery.

Pour info, [un compte-rendu de ce que nous avions fait à Itterswiller en janvier 2023](ttps://bipodocrates.frama.io/alsace/apero-info/2023/01/20/apero-info-itterswiller.html) est disponible.
