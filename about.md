---
layout: page
title: A propos
permalink: /a-propos/
---

*Les Bipodocrates* est une organisation intervenant dans les villages, et visant à l’émancipation des citoyen·ne·s. Le monde tel qu’il est ne lui convient pas. Elle propose de créer des espaces collectifs de discussion, de réflexion et de débat qui mènent à des actions pour tenter de changer les lignes.

Cette organisation est composée actuellement de personnes ayant des compétences en matière de facilitation, de développement de sites web, d'écriture, bref des artistes au service des citoyen·ne·s pour leur redonner le pouvoir.

![logo bipodocrate](/assets/logo.png "logo")
